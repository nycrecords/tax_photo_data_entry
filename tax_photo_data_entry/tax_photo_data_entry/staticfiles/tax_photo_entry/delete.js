

// CSRF code


$(document).ready(function(){
	$(document).on('click', '.delete_button', function(){
		photo_id = this.id
		$.ajax({	
			type: "DELETE",
			url:"/tax_photo_entry/delete_single_dc_data/" +photo_id +"/",
			//url:"edit/",
			beforeSend: function(xhr, settings) {		
            	xhr.setRequestHeader("Authorization", 'Token 3e9d89224a1b1524e36fc34116e6c840bc8756e1');		
    		},
            success:function(data){
				alert('Delete Confirmed')
			}
		});
		$.ajax({
				
			type: "DELETE",
			url:"/tax_photo_entry/delete_single_photo_data/" +photo_id +"/",
			//url:"edit/",
			beforeSend: function(xhr, settings) {		
            	xhr.setRequestHeader("Authorization", 'Token 3e9d89224a1b1524e36fc34116e6c840bc8756e1');		
    		},
            success:function(data){
				
			}
		});
	})

    $(document).on('click', '.ignore_delete', function() {
        photo_id = this.id
        $.ajax({

            type:"PUT",
            url: "/tax_photo_entry/update_single_photo/" +photo_id +"/",
            data:{ "marked_delete":false},
                
            beforeSend: function(xhr, settings) {
                    
                xhr.setRequestHeader("Authorization", 'Token 3e9d89224a1b1524e36fc34116e6c840bc8756e1');
                    
            },
            success:function(data) {
                alert('Lot Saved')
            }

        })

    })


	// CSRF code
    // using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
    console.log(csrftoken)

   function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});



});