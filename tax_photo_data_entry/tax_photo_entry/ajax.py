import json
import requests
from django.http import Http404, HttpResponse
from django.views.decorators.csrf import requires_csrf_token
from django.middleware.csrf import get_token
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required


# don't use direct access to request.META.get('CSRF_COOKIE')
# in this case django will NOT send a CSRF cookie. Use get_token function


headers = {'Accept':'application/json', 'content_type':'application/json', 'Authorization': 'Token dc62f54d8c9a17307498975aec178d0a347d46d4'}

def edit_photo_form(request):


	if request.is_ajax():
		csrftoken = request.POST.get('csrfmiddlewaretoken')
		auto_photo_id = request.POST.get('auto_photo_id')
		photo_identifier = request.POST.get('photo_identifier')
		borough = request.POST.get('borough')
		block = request.POST.get('block')
		lot = request.POST.get('lot')
		building_number = request.POST.get('building_number')
		street_name = request.POST.get('street_name')
		zip_code = request.POST.get('zip_code')
		coverage_name = request.POST.get('coverage_name')
		description = request.POST.get('description')
		photo_url = 'https://records-tax-photo.herokuapp.com/tax_photo_entry/update_single_photo/'+str(auto_photo_id)+'/'
		edit_headers = {'Accept':'application/json', 'content_type':'application/json', 'Authorization': 'Token dc62f54d8c9a17307498975aec178d0a347d46d4', 'X-CSRFToken': csrftoken, 'Referer': photo_url}
		photo_new_info = {'photo_identifier':photo_identifier, 'borough':borough, 'block':block, 'lot':lot, 'building_number':building_number,
		'street_name':street_name, 'zip_code':zip_code}

		return HttpResponse('yay', content_type='application/json')
	else:
		raise Http404

def filter_photo_form(request):

	if request.is_ajax():

		borough = request.POST.get('borough')
		block = request.POST.get('block')
		lot = request.POST.get('lot')
		building_number = request.POST.get('building_number')
		street_name = request.POST.get('street_name')
		zip_code = request.POST.get('zip_code')

		if not lot:
			lot='NA'
		if not borough:
			borough='NA'
		if not block:
			block='NA'
		if not building_number:
			building_number='NA'
		if not street_name:
			street_name='NA'
		if not zip_code:
			zip_code='NA'

		full_data = {}
		url = 'http://records-tax-photo.herokuapp.com/tax_photo_entry/photos/'+borough+'/'+block+'/' + lot+ '/' + building_number + '/' + street_name + '/' + zip_code+ '/'
		dc_url = 'http://records-tax-photo.herokuapp.com/tax_photo_entry/filter_dublin_core/'+borough+'/'+block+'/' + lot+ '/' + building_number + '/' + street_name + '/' + zip_code+ '/'

		r = requests.get(url, headers=headers)
		dc_r = requests.get(dc_url, headers=headers)
		data = r.text
		data = json.loads(data)
		dc_data = dc_r.text
		dc_data = json.loads(dc_data)

		full_data['photo_metadata'] = data
		full_data['dublin_core'] = dc_data


		return HttpResponse(json.dumps(full_data), content_type='application/json')
	else:
		raise Http404
