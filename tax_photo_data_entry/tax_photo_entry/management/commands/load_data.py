import os
import csv
from django.conf import settings
#from settings import BASE_DIR
from django.core.management.base import BaseCommand
from tax_photo_entry.models import Photo_Metadata, Dublin_Core
import requests

metadata_defaults = {"title_alternative": "New York City Tax Photograph Collection", "temporal_coverage": "1939-1941",
    "temporal_start": "1939", "temporal_end": "1941", "abstract": "The New York City Tax Photograph Collection documents nearly every building within Manhattan, Queens, Brooklyn, Bronx, and Staten Island between 1939- 1940.",
    "type": "Stillimage", "isformatof": "35mm nitrate negative and photographic prints","source": "Metadata is in part derived from the NYC Department of Finance's Real Property Assessment Data (RPAD).",
    "contributor": "New York (N.Y.) Municipal Archives", "publisher": "New York(N.Y.) Dept. of Records and Information Services, Municipal Archives/31 Chambers St., Room 101, New York, New York, United States 10007",
    "rights": "The Municipal Archives does not determine the copyright status  on materials, instead copyright permissions are the responsibility of the researcher.  The Municipal Archives requests full and proper credit to cite sources:   Courtesy of NYC Municipal Archives.",
    "format": "image/tiff", "language": "eng", }
city_defaults = {"Manhattan":"New York", "Brooklyn":"Brooklyn", "Queens":"Queens", "Bronx":"Bronx", "Staten Island":"Staten Island"}

class Command(BaseCommand):

	def handle(self, *args, **options):
		loadPhotoData()

def test_api():
	url = 'http://127.0.0.1:8000/tax_photo_entry/update_single_dc_data/nynyma_tax_1_00001_0010/'
	payload = {'abstract': 'bob', 'coverage_name': 'bob@bob.com', 'is_format_of':'words'}
	r = requests.put(url, data=payload)
	data = r.status_code

def loadPhotoData():
	with open(os.path.join(settings.BASE_DIR, 'Tax_Queens.csv'), 'rb') as f:
		next(f)
		reader = csv.reader(f)
		#print row
		for row in reader:
			print row
			c = Photo_Metadata(photo_identifier = row[0],
				borough = row[1], block = int(float(row[2])), lot = int(float(row[3])),
				building_number = row[4], street_name = row[5],
				zip_code = int(float(row[6])), lot_frontage = row[7], lot_depth = row[8],
				year_built = row[9], year_altered_one = row[10],
				year_altered_two = row[11], date=row[12], notes=row[13], description=row[14])
			c.save()
			d = Dublin_Core(photo = c.auto_photo_id, borough = c.borough, block = c.block, lot = c.lot, title = getTitle(c), title_alternative = metadata_defaults["title_alternative"], coverage_temporal = metadata_defaults["temporal_coverage"],
			start = metadata_defaults["temporal_start"], end = metadata_defaults["temporal_end"], building_number =c.building_number, street_name = c.street_name, abstract = metadata_defaults["abstract"], description = c.description,
			city = city_defaults[c.borough], zip_code = c.zip_code, type_property = metadata_defaults["type"], is_format_of = metadata_defaults["isformatof"], source = metadata_defaults["source"],
			contributor = metadata_defaults["contributor"], publisher = metadata_defaults["publisher"], rights = metadata_defaults["rights"],
			resource_format = metadata_defaults["format"], photo_identifier = getIdentifier(c))
			d.save()



def loadDublinCore():
	photos = Photo_Metadata.objects.all()
	for p in photos:


		d = Dublin_Core(photo = p.auto_photo_id, borough = p.borough, block = p.block, lot = p.lot, title = getTitle(p), title_alternative = metadata_defaults["title_alternative"], coverage_temporal = metadata_defaults["temporal_coverage"],
			start = metadata_defaults["temporal_start"], end = metadata_defaults["temporal_end"], building_number = p.building_number, street_name = p.street_name, abstract = metadata_defaults["abstract"],
			city = city_defaults[p.borough], zip_code = p.zip_code, type_property = metadata_defaults["type"], is_format_of = metadata_defaults["isformatof"], source = metadata_defaults["source"],
			contributor = metadata_defaults["contributor"], publisher = metadata_defaults["publisher"], rights = metadata_defaults["rights"],
			resource_format = metadata_defaults["format"], photo_identifier = getIdentifier(p))
		d.save()


def testAPI():
	url = 'http://127.0.0.1:8000/tax_photo_entry/photos/1/101/'
	r = requests.get(url)






def getTitle(photo):
	borough = photo.borough
	block = photo.block
	lot = photo.lot
	title = str(block) + ", " + str(lot) + ", "  + borough
	return title

def getCoverageSpatial(photo):
	building_number = photo.building_number
	street_name = photo.street_name
	city = "New York"
	state = "NY"
	zip_code = photo.zip_code
	spatial_coverage = str(building_number) + ", " + str(street_name) + ", " + city + ", " + state + ", " + str(zip_code)
	return spatial_coverage

def getIdentifier(photo):
	return photo.photo_identifier

