from __future__ import unicode_literals

from django.apps import AppConfig


class TaxPhotoEntryConfig(AppConfig):
    name = 'tax_photo_entry'
