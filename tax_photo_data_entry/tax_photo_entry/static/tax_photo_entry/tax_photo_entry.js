

var metadata_defaults = {"title_alternative": "New York City Tax Photograph Collection", "temporal_coverage": "1939-1941",
    "temporal_start": "1939", "temporal_end": "1941", "abstract": "The New York City Tax Photograph Collection documents nearly every building within Manhattan, Queens, Brooklyn, Bronx, and Staten Island between 1939- 1940.",
    "type": "Stillimage", "isformatof": "35mm nitrate negative and photographic prints","source": "Metadata is in part derived from the NYC Department of Finance's Real Property Assessment Data (RPAD).",
    "contributor": "New York (N.Y.) Municipal Archives", "publisher": "New York(N.Y.) Dept. of Records and Information Services, Municipal Archives/31 Chambers St., Room 101, New York, New York, United States 10007",
    "rights": "The Municipal Archives does not determine the copyright status  on materials, instead copyright permissions are the responsibility of the researcher.  The Municipal Archives requests full and proper credit to cite sources:   Courtesy of NYC Municipal Archives.",
    "format": "image/tiff", "language": "eng", }
var city_defaults = {"Manhattan":"New York", "Brooklyn":"Brooklyn", "Queens":"Queens", "Bronx":"Bronx", "Staten Island":"Staten Island"}

function createDialogForm(id) {
    if(id) {
        $.ajax({	
			type: "GET",
			url:"get_photo_w_id/" + id +"/",
			beforeSend: function(xhr, settings) {
            	xhr.setRequestHeader("Authorization", 'Token dc62f54d8c9a17307498975aec178d0a347d46d4');	
    		},
            success:function(data){
            	$('#new_borough').val(data[0]['borough'])
            	$('#new_block').val(data[0]['block'])
            	$('#new_lot').val(data[0]['lot'])
            	$('#new_building_number').val(data[0]['building_number'])
            	$('#new_street_name').val(data[0]['street_name'])
            	$('#new_zip_code').val(data[0]['zip_code'])
            	$('#new_landmark_name').val(data[0]['landmark_name'])
            	$('#new_lot_frontage').val(data[0]['lot_frontage'])
            	$('#new_lot_depth').val(data[0]['lot_depth'])
            	$('#new_year_built').val(data[0]['year_built'])
            	$('#new_year_altered_one').val(data[0]['year_altered_one'])
            	$('#new_year_altered_two').val(data[0]['year_altered_two'])
            	$('#new_date').val(data[0]['date'])
            	$('#new_photo_identifier').val(data[0]['photo_identifier'])
            	$('#new_coverage_name').val(data[0]['coverage_name'])
       			$('#new_description').val(data[0]['description'])
       			$('#new_notes').val(data[0]['notes'])
            	$("#create_new_dialog" ).dialog( "open" );
			}
		});
    } else {
       $('#new_borough').val('')
       $('#new_block').val('')
       $('#new_lot').val('')
       $('#new_building_number').val('')
       $('#new_street_name').val('')
       $('#new_zip_code').val('')
       $('#new_landmark_name').val('')
       $('#new_lot_frontage').val('')
       $('#new_lot_depth').val('')
       $('#new_year_built').val('')
       $('#new_year_altered_one').val('')
       $('#new_year_altered_two').val('')
       $('#new_date').val('1939-1941')
       $('#new_photo_identifier').val('')
       $('#new_coverage_name').val('')
       $('#new_description').val('')
       $('#new_notes').val('')
       $( "#create_new_dialog" ).dialog( "open" );
    }         
}

function createNew() {
	error_message = checkInput();
	if(error_message.length > 0) {
		alert(error_message)
	} else {
		new_photo_data = {'street_name':$('#new_street_name').val(), 'building_number':$('#new_building_number').val(), 'lot':$('#new_lot').val(),
		'borough':$('#new_borough').val(), 'block':$('#new_block').val(),'zip_code':$('#new_zip_code').val(), 'landmark_name':$('#new_landmark_name').val(), 'lot_frontage':$('#new_lot_frontage').val(),
		'lot_depth':$('#new_lot_depth').val(), 'year_built':$('#new_year_built').val(), 'year_altered_one':$('#new_year_altered_one').val(),
		'year_altered_two':$('#new_year_altered_two').val(), 'date':$('#new_date').val(), 'photo_identifier':$('#new_photo_identifier').val(),
		'description': $('#new_description').val(),'coverage_name': $('#new_coverage_name').val(), 'notes':$('#new_notes').val()}
		$.ajax({		
			type: "POST",
			url: "photos/",
			data: new_photo_data,
			beforeSend: function(xhr, settings) {		
            	xhr.setRequestHeader("Authorization", 'Token dc62f54d8c9a17307498975aec178d0a347d46d4');		
    		},
            success:function(data){
            	auto_photo_id = data['auto_photo_id']
            	new_dublin_core_data = {'photo': auto_photo_id, 'lot': data['lot'], 'block': data['block'], 'borough': data['borough'], 'zip_code':data['zip_code'],
                    'description': data['description'],'coverage_name': data['coverage_name'], 'coverage_temporal': metadata_defaults['temporal_coverage'], 'landmark_name':data['landmark_name'],
                    'building_number': data['building_number'], 'city': city_defaults[data['borough']],'street_name': data['street_name'], 'title_alternative': metadata_defaults['title_alternative'],
                    'start': metadata_defaults['temporal_start'], 'end': metadata_defaults['temporal_end'],'abstract': metadata_defaults['abstract'], 'type_property': metadata_defaults['type'],
                    'is_format_of': metadata_defaults['isformatof'],'source': metadata_defaults['source'], 'contributor': metadata_defaults['contributor'],
                    'publisher': metadata_defaults['publisher'],'rights': metadata_defaults['rights'], 'photo_identifier':data['photo_identifier']}
            	$.ajax({
					type: "POST",
					url:"post_new_dublin_core/" ,
					data:new_dublin_core_data,
					beforeSend: function(xhr, settings) {
            			xhr.setRequestHeader("Authorization", 'Token dc62f54d8c9a17307498975aec178d0a347d46d4');
    				},
           			success:function(data){
            		alert("Entry added successfully.")

            		}
				});	
			}
		});
	}
}

function autoMarkDelete(photo_id) {
	if($('#checkbox_' + photo_id ).is(':checked')) {
		$.ajax({	
			type: "PUT",
			url:"update_single_photo/" +photo_id +"/",
			data: {"marked_delete":true},
			beforeSend: function(xhr, settings) {		
            	xhr.setRequestHeader("Authorization", 'Token dc62f54d8c9a17307498975aec178d0a347d46d4');		
    		},
            success:function(data){	
				
			}
		});
	}
}

function autoSave(element_id){
	photo_id = element_id.split('-')[0]
	element_name = element_id.split('-')[1]
	photo_data = {}
	photo_data[element_id.split('-')[1]] = $('#' + element_id).val()
	dc_data = {}
	if(element_name == 'borough'){
		dc_data[element_id.split('-')[1]] = $('#' + element_id).val()
		dc_data['city'] = city_defaults[$('#' + element_id).val()]
	} else{
		dc_data[element_id.split('-')[1]] = $('#' + element_id).val()
	}
	if($('#' + element_id).val() <= 0) {
		alert(element_name + " cannot be left blank")
	} else {
		$.ajax({		
			type: "PUT",
			url:"update_single_photo/" +photo_id +"/",
			data: photo_data,
			beforeSend: function(xhr, settings) {		
            	xhr.setRequestHeader("Authorization", 'Token dc62f54d8c9a17307498975aec178d0a347d46d4');		
    		},
            success:function(data){
			}
		});

		$.ajax({	
			type: "PUT",
			url:"update_single_dc_data/"+photo_id+"/" ,
			data:dc_data,
			beforeSend: function(xhr, settings) {		
            	xhr.setRequestHeader("Authorization", 'Token dc62f54d8c9a17307498975aec178d0a347d46d4');		
    		},
            success:function(data){
            }
		});
	}			
}

function checkInput(){
	error_message = ""
	if ($('#new_street_name').val() == ""){ 
		error_message += "Street Name is a required field. \n"
	}
	if ($('#new_building_number').val() == ""){ 
		error_message += "Building Number is a required field. \n"
	}
	if ($('#new_lot').val() == ""){ 
		error_message += "Lot Number is a required field. \n"
	}
	if ($('#new_borough').val() == ""){ 
		error_message += "Borough is a required field. \n"
	}
	if ($('#new_block').val() == ""){ 
		error_message += "Block Number is a required field. \n"
	} 
	/*else if (!(/\d/).test($('#new_block').val()) && $('#new_block').val() != "VOID"){
		error_message += "Block Number is not a valid entry. \n"
	}*/
	if ($('#new_zip_code').val() == ""){ 
		error_message += "Zip Code is a required field. \n"
	}
	else if ($('#new_zip_code').val() != "VOID" && isNaN($('#new_zip_code').val())){
		error_message += "Zip Code is not a valid entry. \n"
	}
	if ($('#new_lot_frontage').val() != "" && $('#new_lot_frontage').val() != "VOID" && !(!isNaN($('#new_lot_frontage').val()) && $('#new_lot_frontage').val().indexOf('.') != -1)){
		error_message += "Lot Frontage is not a valid entry. \n"
	}
	if ($('#new_lot_depth').val() != "" && $('#new_lot_depth').val() != "VOID" && !(!isNaN($('#new_lot_depth').val()) && $('#new_lot_depth').val().indexOf('.') != -1)){
		error_message += "Lot Depth is not a valid entry. \n"
	}
	return error_message;
}


$(document).ready(function(){
	var display_photo_metadata = ['lot_frontage', 'lot_depth', 'year_built', 'year_altered_one', 'year_altered_two', 'date', 'auto_photo_id', 'marked_delete']
	var edit_photo_metadata =  ['borough', 'block', 'lot', 'street_name', 'building_number','zip_code', 'landmark_name','photo_identifier', 'description', 'coverage_name', 'notes']

	$('#submit_filter').click(function(){
		$.ajax({
			type:"POST",
			url:"filter/",
			dataType:"json",
			data:{"borough": $("#borough").val(), "block":$("#block").val(), "lot":$("#lot").val(), "building_number":$("#building_number").val(),
			"street_name":$("#street_name").val(), "zip_code":$("#zip_code").val()},
			success: function(data) {
				if(data['photo_metadata'].length > 0 && data['dublin_core'].length > 0) {
					$('#search_results').empty();
					var table = '';
					var header = '<tr><td>Borough</td><td>Block</td><td>Lot</td><td>Street Name</td><td>Buiding Number'+
						'</td><td>Zip Code</td><td>Landmark Name</td><td>Photo Identifier</td><td>Description</td><td>Coverage Name</td>'+
						'<td>Notes</td><td>Lot Frontage</td><td>Lot Depth</td><td>Year Built</td><td>Year Altered One</td><td>Year Altered Two</td>'+
						'<td>Date</td><td>Auto Generated ID</td><td>Marked Delete</td><td>Check to Delete</td></tr>'
					table += header
					photo_metadata = data['photo_metadata']
					dublin_core = data['dublin_core']
					for(i = 0; i < photo_metadata.length; i++) {
						var photo_id = photo_metadata[i]['auto_photo_id']
						table += '<tr>';
						for(j = 0; j < edit_photo_metadata.length; j++) {	
							var id = photo_id + '-' + edit_photo_metadata[j]
							table += '<td><input class="edit_fields" onchange=autoSave(\'' +id+ '\') id= \'' + id+ '\' type=text name= \'' + id+ '\'value=\'' + photo_metadata[i][edit_photo_metadata[j]] + '\'/></td>'
						}
						for(j = 0; j < display_photo_metadata.length; j++) {
							var id = photo_id + '-' + display_photo_metadata[j]
							table += '<td>' + photo_metadata[i][display_photo_metadata[j]]+ '</td>';
						}	
						table += '<td><input onchange=autoMarkDelete(\'' +photo_id+ '\') id= \'checkbox_' + photo_id+ '\' type=checkbox name= \'' + photo_id+ '\'/></td>'
						table += '<td><button class="duplicate" onclick=createDialogForm(\'' + photo_id +'\')>Duplicate Entry</button></td></tr>'
					}
					$('#search_results').append(table);
            		} else {
            			alert("This entry was not found")
            		}
        		}
			});
		});


	
	function getCookie(name) {
    	var cookieValue = null;
    	if (document.cookie && document.cookie != '') {
        	var cookies = document.cookie.split(';');
        	for (var i = 0; i < cookies.length; i++) {
            	var cookie = jQuery.trim(cookies[i]);
            	// Does this cookie string begin with the name we want?
            	if (cookie.substring(0, name.length + 1) == (name + '=')) {
                	cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                	break;
            	}
        	}
    	}
    	return cookieValue;
	}

	var csrftoken = getCookie('csrftoken');

   		function csrfSafeMethod(method) {
    	// these HTTP methods do not require CSRF protection
    	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
	$.ajaxSetup({
    	beforeSend: function(xhr, settings) {
        	if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            	xhr.setRequestHeader("X-CSRFToken", csrftoken);
        	}
    	}
	});

});