

// CSRF code


$(document).ready(function(){
	var display_photo_metadata = ['lot_frontage', 'lot_depth', 'year_built', 'year_altered_one', 'year_altered_two', 'date']
	var edit_photo_metadata =  ['photo_identifier', 'borough', 'block', 'lot', 'building_number', 'street_name', 'zip_code']
	var edit_dublin_core  = ['coverage_name', 'description' ]
	var city_defaults = {"Manhattan":"New York", "Brooklyn":"Brooklyn", "Queens":"Queens", "Bronx":"Bronx", "Staten Island":"Staten Island"}
	$(document).on('click', '.edit_button', function(){
		photo_id = this.id
		var to_delete = false;
		if($('#checkbox_' + photo_id ).is(':checked')) {
			to_delete = true;
		}
		data = {"photo_identifier": $('#' + photo_id + '_photo_identifier').val(),"borough": $('#' + photo_id + '_borough').val(), 
		"block": $('#' + photo_id + '_block').val(), "lot": $('#' + photo_id + '_lot').val(), "building_number": $('#' + photo_id + '_building_number').val(),
		"street_name": $('#' + photo_id + '_street_name').val(), "zip_code": $('#' + photo_id + '_zip_code').val(),
		"coverage_name": $('#' + photo_id + '_coverage_name').val(), "description": $('#' + photo_id + '_description').val(), "marked_delete":to_delete, csrfmiddlewaretoken: csrftoken}

		all_required = true;
		if($('#' + photo_id + '_photo_identifier').val() == ""){	
			alert('The General Photo Identifier field cannot be blank')
		} else if ($('#' + photo_id + '_street_name').val() == ""){ 
			alert('The Street Name field cannot be left blank')
		} else if ($('#' + photo_id + '_building_number').val() == ""){ 
			alert('The Building Number field cannot be left blank')
		} else if ($('#' + photo_id + '_lot').val() == ""){ 
			alert('The Lot Number field cannot be left blank')
		} else if ($('#' + photo_id + '_borough').val() == ""){ 
			alert('The Borough field cannot be left blank')
		} else if ($('#' + photo_id + '_block').val() == ""){ 
			alert('The Block field cannot be left blank')
		} else if ($('#' + photo_id + '_zip_code').val() == ""){ 
			alert('The Zip Code field cannot be left blank')
		} else {
	
			$.ajax({
				type: "PUT",
				url:"update_single_photo/" +photo_id +"/",
				data:{"auto_photo_id":photo_id,"photo_identifier": $('#' + photo_id + '_photo_identifier').val(),"borough": $('#' + photo_id + '_borough').val(), 
					"block": $('#' + photo_id + '_block').val(), "lot": $('#' + photo_id + '_lot').val(), "building_number": $('#' + photo_id + '_building_number').val(),
					"street_name": $('#' + photo_id + '_street_name').val(), "zip_code": $('#' + photo_id + '_zip_code').val(),
					"coverage_name": $('#' + photo_id + '_coverage_name').val(), "marked_delete":to_delete, "description": $('#' + photo_id + '_description').val()},
				beforeSend: function(xhr, settings) {	
            		xhr.setRequestHeader("Authorization", 'Token dc62f54d8c9a17307498975aec178d0a347d46d4');		
    			},
            	success:function(data){
					alert('Edits Saved')
				}
			});

			$.ajax({
				type: "PUT",
				url:"update_single_dc_data/"+photo_id+"/" ,
				data:{'title_lot':$('#' + photo_id + '_lot').val(), 'title_block':$('#' + photo_id + '_block').val(), 'title_borough':$('#' + photo_id + '_borough').val(), 'description':$('#' + photo_id + '_description').val(), 
					'coverage_name':$('#' + photo_id + '_coverage_name').val(), 'coverage_spatial_building': $('#' + photo_id + '_building_number').val(),
					'coverage_spatial_street':$('#' + photo_id + '_street_name').val(), 'city': city_defaults[$('#' + photo_id + '_borough').val()]},
				beforeSend: function(xhr, settings) {		
            		xhr.setRequestHeader("Authorization", 'Token dc62f54d8c9a17307498975aec178d0a347d46d4');	
    			},
            	success:function(data){
            	
            	}
			});
		}
	});

	// CSRF code
    // using jQuery
	function getCookie(name) {
    	var cookieValue = null;
    	if (document.cookie && document.cookie != '') {
        	var cookies = document.cookie.split(';');
        	for (var i = 0; i < cookies.length; i++) {
            	var cookie = jQuery.trim(cookies[i]);
            	// Does this cookie string begin with the name we want?
            	if (cookie.substring(0, name.length + 1) == (name + '=')) {
                	cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                	break;
            	}
        	}
    	}
    	return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

   	function csrfSafeMethod(method) {
    	// these HTTP methods do not require CSRF protection
    	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
	$.ajaxSetup({
    	beforeSend: function(xhr, settings) {
        	if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            	xhr.setRequestHeader("X-CSRFToken", csrftoken);
        	}
    	}
	});



});